$(document).ready(function () {
    // autocomplete
    $("#id_search").autocomplete({
        source: "/auto/",
        minLength: 3,
        open: function(){
            setTimeout(function(){
                $('.ui-autocomplete').css('z-index', 9);
            }, 0);
        }
    });

    $('#kartaSearch .errorlist').hide();

    var cislo = $("#novy_stav > span").text();
    cislo = parseInt(cislo);
    if (cislo == 9) {
        $('#puvodni_stav > strong, #novy_stav > span').addClass('redAlert');
    };

    var cislo_do_50 = $("#novy_stav_do_50 > span").text();
    cislo = parseInt(cislo_do_50);
    if (cislo_do_50 == 9) {
        $('#puvodni_stav_do_50 > strong, #novy_stav_do_50 > span').addClass('redAlert');
    };

    // nový stav na kartě do 50kč
    $("#novy_stav_do_50 > button").click(function(event){
        var cislo = $("#novy_stav_do_50 > span").text();
        cislo = parseInt(cislo);
        var foo;

        if (event.target.id == "minus_do_50") {
            foo = cislo - 1;
        } else if (event.target.id == "plus_do_50") {
            foo = cislo + 1;
        };

        if (foo == 9) {
            // set red background
            $('#novy_stav_do_50 > span').addClass('redAlert');
        } else if (foo >= 10) {
            foo = 0;
            $('#novy_stav_do_50 > span').removeClass('redAlert');
        } else if (foo <= -1) {
            // set red background
            $('#novy_stav_do_50 > span').addClass('redAlert');
            foo = 9;
        } else {
            $('#novy_stav_do_50 > span').removeClass('redAlert');
        };

        // zobraz upravenou hodnotu 
        $("#novy_stav_do_50 > span").text(foo);
        // nastav upravenou hodnotu do formulářového pole k propsání do DB
        $("#id_stav_do_50").val(foo);
        return false;
    });

    // nový stav na kartě
    $("#novy_stav > button").click(function(event){
        var cislo = $("#novy_stav > span").text();
        cislo = parseInt(cislo);
        var foo;

        if (event.target.id == "minus") {
            foo = cislo - 1;
        } else if (event.target.id == "plus") {
            foo = cislo + 1;
        };

        if (foo == 9) {
            // set red background
            $('#novy_stav > span').addClass('redAlert');
        } else if (foo >= 10) {
            foo = 0;
            $('#novy_stav > span').removeClass('redAlert');
        } else if (foo <= -1) {
            // set red background
            $('#novy_stav > span').addClass('redAlert');
            foo = 9;
        } else {
            $('#novy_stav > span').removeClass('redAlert');
        };

        // zobraz upravenou hodnotu 
        $("#novy_stav > span").text(foo);
        // nastav upravenou hodnotu do formulářového pole k propsání do DB
        $("#id_stav").val(foo);
        return false;
    });

});
