from django.db import models
from django.db.models import Q
from django.core.validators import RegexValidator


class Karta(models.Model):
    cislo = models.SmallIntegerField(unique=True, help_text='Číslo karty')
    cislo_str = models.CharField(
            validators=[RegexValidator(regex='^.{5}$', message='Délka musí být 5 znaků', code='nomatch')],
            max_length=5, unique=True, help_text='Karta č.')
    nick = models.CharField(max_length=55, blank=True, help_text='Jméno držitele karty')
    celkem_nakupu = models.SmallIntegerField(default=0, help_text='Celkový počet všech nákupů nad 50 kč')
    stav = models.SmallIntegerField(default=0, help_text='Počet bodů na kartě 0-9')
    celkem_nakupu_do_50 = models.SmallIntegerField(default=0, help_text='Celkový počet všech nákupů')
    stav_do_50 = models.SmallIntegerField(default=0, help_text='Počet bodů na kartě 0-9, nákup do 50 kč')
    aktivni = models.BooleanField(
        default=True, help_text='Pokud se karta ztratí nebo bude smazána, nastavit False')
    datum_zalozeni = models.DateTimeField(
        auto_now_add=True, help_text='Datum aktivace a předání karty držiteli')
    datum_posledni_zmeny = models.DateTimeField(
        auto_now=True, help_text='Datum poslední provedené úpravy na kartě')

    class Meta:
        ordering = ['-datum_posledni_zmeny', '-cislo']
        verbose_name = 'karta'
        verbose_name_plural = 'karty'

    def __str__(self):
        return f'karta {self.cislo_str}'

    @classmethod
    def cislo_nove_karty(cls):
        ''' Vrátí nejnižší volné číslo karty v pořadí pro přiřazení k nové kartě'''
        try:
            return cls.objects.all().order_by('cislo').last().cislo + 1
        except AttributeError:
            return 1

    def get_cislo_str(self):
        ''' převod čísla karty do formátu "0000X" '''
        return '{0:0>5}'.format(self.cislo)

    def deaktivuj(self):
        self.aktivni = False
        self.save()

    @classmethod
    def autocomplete(cls, bar):
        ''' Vrátí hodnoty 'číslo_str' a 'nick' jako jediný seznam'''
        data = [j for i in cls.objects.filter(Q(cislo_str__contains=bar)|Q(nick__icontains=bar)).values_list('cislo_str', 'nick') for j in i if j != '']
        return data

    @classmethod
    def generuj_karty(cls, pocet=1):
        ''' Generuj předaný počet nových prázdných karet - vyhni se kolizím'''

        cislo = cls.cislo_nove_karty()
        for i in range(pocet):
            karta = cls(cislo=cislo)
            karta.cislo_str = karta.get_cislo_str()
            karta.save()
            cislo += 1

