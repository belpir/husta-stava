from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.generic.edit import FormView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.db.models import Q
from k2.models import Karta
from .forms import KartaSearchForm


class KartaSearchFormView(LoginRequiredMixin, FormView):
    login_url = '/login/'
    template_name = "k2/karta_search.html"
    form_class = KartaSearchForm
    success_url = "/update/"

    def form_valid(self, form):
        cd = form.cleaned_data
        raw = cd['search']
        if raw:
            try:
                karta = Karta.objects.get(Q(cislo_str=raw)|Q(nick__iexact=raw))
            except (Karta.DoesNotExist, ValueError):
                form.add_error('search', ValidationError('Hledaný výraz nebyl nalezen'))
                return super().form_invalid(form)

            self.success_url = f'/update/{karta.pk}'
        return super().form_valid(form)


class KartaUpdate(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    model = Karta
    fields = ['nick', 'stav', 'stav_do_50']
    template_name = 'k2/karta_update.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs['pk']
        context['karta'] = self.model.objects.get(pk=pk)
        return context

    def form_valid(self, form, **kwargs):
        cd = form.cleaned_data
        nick = cd.get('nick')
        if nick:
            if nick.isnumeric():
                form.add_error('nick', ValidationError('Přezdívka se nesmí skládat pouze z čísel'))
                return super().form_invalid(form)
            if len(nick) < 3:
                form.add_error('nick', ValidationError('Minimální délka přezdívky jsou 3 znaky'))
                return super().form_invalid(form)
            # validace duplicitnich nicku, vynech sam sebe
            dupl_qs = self.model.objects.filter(nick__iexact=nick)
            if dupl_qs:
                dupl = dupl_qs.first()
                if dupl.pk != form.instance.pk:
                    form.add_error('nick', ValidationError('Tato přezdívka již existuje'))
                    return super().form_invalid(form)

        return super().form_valid(form)


def KartaAutocompleteView(request):
    if request.is_ajax():
        dotaz = request.GET.get('term', '')
        data = Karta.autocomplete(dotaz) if dotaz else  Karta.autocomplete(5)
    else:
        data = 'fail'

    return JsonResponse(data, safe=False)

