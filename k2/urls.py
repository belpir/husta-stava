from django.urls import path
from k2 import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.KartaSearchFormView.as_view(), name='karta-search'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('update/<int:pk>', views.KartaUpdate.as_view(), name='karta-update'),
    path('auto/', views.KartaAutocompleteView, name='karta-auto'),
]

