#! /usr/bin/env python3

from k2.models import Karta
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Add new bunch of predefined cards'

    def add_arguments(self, parser):
        parser.add_argument('amount', type=int, help='Kolik novych karet bude pridano')

    def handle(self, *args, **kwargs):
        amount = kwargs['amount']
        if amount:
            Karta.generuj_karty(amount)
            self.stdout.write(self.style.SUCCESS(f'Všech {amount} nových karet bylo úspěšne vytvořeno'))
            self.stdout.write(self.style.SUCCESS(f'Celkový počet karet je {Karta.objects.count()}'))

